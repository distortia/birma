defmodule Birma.PantryTest do
  use Birma.DataCase

  alias Birma.Pantry

  describe "items" do
    alias Birma.Pantry.Item

    @valid_attrs %{name: "some name", type: "some type"}
    @update_attrs %{name: "some updated name", type: "some updated type"}
    @invalid_attrs %{name: nil, type: nil}

    def item_fixture(attrs \\ %{}) do
      {:ok, item} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Pantry.create_item()

      item
    end

    test "list_items/0 returns all items" do
      item = item_fixture()
      assert Pantry.list_items() == [item]
    end

    test "get_item!/1 returns the item with given id" do
      item = item_fixture()
      assert Pantry.get_item!(item.id) == item
    end

    test "create_item/1 with valid data creates a item" do
      assert {:ok, %Item{} = item} = Pantry.create_item(@valid_attrs)
      assert item.name == "some name"
      assert item.type == "some type"
    end

    test "create_item/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Pantry.create_item(@invalid_attrs)
    end

    test "update_item/2 with valid data updates the item" do
      item = item_fixture()
      assert {:ok, %Item{} = item} = Pantry.update_item(item, @update_attrs)
      assert item.name == "some updated name"
      assert item.type == "some updated type"
    end

    test "update_item/2 with invalid data returns error changeset" do
      item = item_fixture()
      assert {:error, %Ecto.Changeset{}} = Pantry.update_item(item, @invalid_attrs)
      assert item == Pantry.get_item!(item.id)
    end

    test "delete_item/1 deletes the item" do
      item = item_fixture()
      assert {:ok, %Item{}} = Pantry.delete_item(item)
      assert_raise Ecto.NoResultsError, fn -> Pantry.get_item!(item.id) end
    end

    test "change_item/1 returns a item changeset" do
      item = item_fixture()
      assert %Ecto.Changeset{} = Pantry.change_item(item)
    end
  end

  describe "meals" do

    test "generate_meal/0 returns 1 meat 2 veggies" do
      {:ok, item} = Pantry.create_item(%{"name" => "spinach", "type" => "vegetable"})
      {:ok, item2} = Pantry.create_item(%{"name" => "potato", "type" => "vegetable"})
      {:ok, item3} = Pantry.create_item(%{"name" => "chicken thighs", "type" => "meat"})

      expected = [
        item, item2, item3
      ]
      meal = Pantry.generate_meal()

      assert Enum.any?(expected, fn e -> e in meal end)
    end

    test "generate_meals/0 returns a list of meals" do
      {:ok, item} = Pantry.create_item(%{"name" => "spinach", "type" => "vegetable"})
      {:ok, item2} = Pantry.create_item(%{"name" => "potato", "type" => "vegetable"})
      {:ok, item3} = Pantry.create_item(%{"name" => "chicken thighs", "type" => "meat"})

      expected = [
        item, item2, item3
      ]
      meals = Pantry.generate_meals()

      assert is_list(meals)
      assert is_list(List.first(meals))
      assert Enum.any?(expected, fn e -> e in List.first(meals) end)
    end
  end
end

defmodule BirmaWeb.PantryResolverTest do
  use BirmaWeb.ConnCase
  alias Birma.Pantry

  @item %{name: "ribeye", type: "meat"}

  describe "Pantry Resolver" do
    test "all_items/0", %{conn: conn} do
      {:ok, item} = Pantry.create_item(@item)
      {:ok, item2} = Pantry.create_item(%{name: "spinach", type: "vegetable"})
      query = """
        {
          allItems {
            id
          }
        }
      """

      res = conn |> get("/api/graphiql", %{query: query}) |> json_response(200)

      assert res["data"]["allItems"] == [%{"id" => "#{item.id}"}, %{"id" => "#{item2.id}"}]
    end
  end

  test "create_item/1", %{conn: conn} do
    query = """
      mutation {
        createItem(
          name: "pork shoulder",
          type: "meat"
        ) {
          name,
          type
        }
      }
    """

    res = conn |> post("/api/graphiql", query: query) |> json_response(200)
    assert res["data"]["createItem"]["name"] == "pork shoulder"
    assert res["data"]["createItem"]["type"] == "meat"
  end

  test "generate_meal/0", %{conn: conn} do
    expected = [
      %{"name" => "ribeye", "type" => "meat"},
      %{"name" => "spinach", "type" => "vegetable"},
      %{"name" => "potato", "type" => "vegetable"}
    ]
    {:ok, _item} = Pantry.create_item(@item)
    {:ok, _item2} = Pantry.create_item(%{name: "spinach", type: "vegetable"})
    {:ok, _item3} = Pantry.create_item(%{name: "potato", type: "vegetable"})
    query = """
      {
        generateMeal {
          name,
          type
        }
      }
    """
    res = conn |> get("/api/graphiql", query: query) |> json_response(200)
    meals = res["data"]["generateMeal"]
    assert Enum.any?(expected, fn item -> item in meals end)
  end

  test "generate_meals/0", %{conn: conn} do
    meal = [
      %{"name" => "ribeye", "type" => "meat"},
      %{"name" => "spinach", "type" => "vegetable"},
      %{"name" => "potato", "type" => "vegetable"}
    ]
    expected_meals = [meal] ++ [meal]

    {:ok, _item} = Pantry.create_item(%{name: "ribeye", type: "meat"})
    {:ok, _item2} = Pantry.create_item(%{name: "spinach", type: "vegetable"})
    {:ok, _item3} = Pantry.create_item(%{name: "potato", type: "vegetable"})

    query = """
      {
        generateMeals {
          name,
          type
        }
      }
    """

    res = conn |> get("/api/graphiql", query: query) |> json_response(200)
    meals = res["data"]["generateMeals"]
    assert Enum.any?(List.first(expected_meals), fn item -> item in List.first(meals) end)
  end
end

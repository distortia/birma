defmodule Birma.Pantry do
  @moduledoc """
  The Pantry context.
  """

  import Ecto.Query, warn: false
  alias Birma.Repo

  alias Birma.Pantry.Item
  alias Birma.Pantry.Meal

  @doc """
  Returns the list of items.

  ## Examples

      iex> list_items()
      [%Item{}, ...]

  """
  def list_items do
    Repo.all(Item)
  end

  @doc """
  Gets a single item.

  Raises `Ecto.NoResultsError` if the Item does not exist.

  ## Examples

      iex> get_item!(123)
      %Item{}

      iex> get_item!(456)
      ** (Ecto.NoResultsError)

  """
  def get_item!(id), do: Repo.get!(Item, id)

  @doc """
  Creates a item.

  ## Examples

      iex> create_item(%{field: value})
      {:ok, %Item{}}

      iex> create_item(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_item(attrs \\ %{}) do
    %Item{}
    |> Item.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a item.

  ## Examples

      iex> update_item(item, %{field: new_value})
      {:ok, %Item{}}

      iex> update_item(item, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_item(%Item{} = item, attrs) do
    item
    |> Item.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Item.

  ## Examples

      iex> delete_item(item)
      {:ok, %Item{}}

      iex> delete_item(item)
      {:error, %Ecto.Changeset{}}

  """
  def delete_item(%Item{} = item) do
    Repo.delete(item)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking item changes.

  ## Examples

      iex> change_item(item)
      %Ecto.Changeset{source: %Item{}}

  """
  def change_item(%Item{} = item) do
    Item.changeset(item, %{})
  end

  @doc """
  Returns a list of pantry items for a meal with 1 meat and 2 veggies

  ## Examples
      iex> generate_meal()
      [
        %Birma.Pantry.Item{
          __meta__: #Ecto.Schema.Metadata<:loaded, "items">,
          id: 5,
          inserted_at: ~N[2019-12-07 21:32:40],
          name: "pork shoulder",
          type: "meat",
          updated_at: ~N[2019-12-07 21:32:40]
        },
        %Birma.Pantry.Item{
          __meta__: #Ecto.Schema.Metadata<:loaded, "items">,
          id: 6,
          inserted_at: ~N[2019-12-07 22:26:43],
          name: "potato",
          type: "vegetable",
          updated_at: ~N[2019-12-07 22:26:43]
        },
        %Birma.Pantry.Item{
          __meta__: #Ecto.Schema.Metadata<:loaded, "items">,
          id: 2,
          inserted_at: ~N[2019-12-07 18:53:50],
          name: "spinach",
          type: "vegetable",
          updated_at: ~N[2019-12-07 18:53:50]
        }
      ]
  """
  def generate_meal() do
    meat = i in Item |> from(where: i.type == "meat") |> Repo.all() |> Enum.shuffle() |> Enum.random()
    veggies = i in Item |> from(where: i.type == "vegetable") |> Repo.all() |> Enum.shuffle() |> Enum.take_random(2)
    [meat] ++ [veggies] |> List.flatten()
  end

  @doc """
  Returns a list of meals containing pantry items

  ## Examples
      iex> generate_meals()
      [
        [
          %Birma.Pantry.Item{
            __meta__: #Ecto.Schema.Metadata<:loaded, "items">,
            id: 5,
            inserted_at: ~N[2019-12-07 21:32:40],
            name: "pork shoulder",
            type: "meat",
            updated_at: ~N[2019-12-07 21:32:40]
          },
          %Birma.Pantry.Item{
            __meta__: #Ecto.Schema.Metadata<:loaded, "items">,
            id: 6,
            inserted_at: ~N[2019-12-07 22:26:43],
            name: "potato",
            type: "vegetable",
            updated_at: ~N[2019-12-07 22:26:43]
          },
          %Birma.Pantry.Item{
            __meta__: #Ecto.Schema.Metadata<:loaded, "items">,
            id: 2,
            inserted_at: ~N[2019-12-07 18:53:50],
            name: "spinach",
            type: "vegetable",
            updated_at: ~N[2019-12-07 18:53:50]
          }
        ],
        [
          %Birma.Pantry.Item{
            __meta__: #Ecto.Schema.Metadata<:loaded, "items">,
            id: 5,
            inserted_at: ~N[2019-12-07 21:32:40],
            name: "pork shoulder",
            type: "meat",
            updated_at: ~N[2019-12-07 21:32:40]
          },
          %Birma.Pantry.Item{
            __meta__: #Ecto.Schema.Metadata<:loaded, "items">,
            id: 6,
            inserted_at: ~N[2019-12-07 22:26:43],
            name: "potato",
            type: "vegetable",
            updated_at: ~N[2019-12-07 22:26:43]
          },
          %Birma.Pantry.Item{
            __meta__: #Ecto.Schema.Metadata<:loaded, "items">,
            id: 2,
            inserted_at: ~N[2019-12-07 18:53:50],
            name: "spinach",
            type: "vegetable",
            updated_at: ~N[2019-12-07 18:53:50]
          }
        ]
      ]
  """
  def generate_meals(number_of_days \\ 2) do
    Enum.map(1..number_of_days, fn _ -> generate_meal() end)
  end
end

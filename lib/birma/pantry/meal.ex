defmodule Birma.Pantry.Meal do
  use Ecto.Schema
  import Ecto.Changeset

  schema "meals" do
    belongs_to(:user, Birma.Veil.User)
    has_many(:items, Birma.Pantry.Item)
    timestamps()
  end

  @doc false
  def changeset(item, attrs) do
    item
    |> cast(attrs, [])
    |> validate_required([])
  end
end

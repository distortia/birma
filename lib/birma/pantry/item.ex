defmodule Birma.Pantry.Item do
  use Ecto.Schema
  import Ecto.Changeset

  schema "items" do
    field :name, :string
    field :type, :string
    field :amount, :integer, default: 0

    belongs_to(:user, Birma.Veil.User)
    belongs_to(:meal, Birma.Pantry.Meal)
    timestamps()
  end

  @doc false
  def changeset(item, attrs) do
    item
    |> cast(attrs, [:name, :type, :amount])
    |> validate_required([:name, :type])
  end
end

defmodule BirmaWeb.Router do
  use BirmaWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug BirmaWeb.Plugs.Veil.UserId
    plug BirmaWeb.Plugs.Veil.User
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", BirmaWeb do
    pipe_through :browser

    get "/", PageController, :index
    get "/catalog", PageController, :catalog
  end

  scope "/pantry" do
    pipe_through [:browser, BirmaWeb.Plugs.Veil.Authenticate]
    get "/", BirmaWeb.PantryController, :index
    get "/manage", BirmaWeb.PantryController, :manage
    post "/add", BirmaWeb.PantryController, :add
    get "/generate", BirmaWeb.PantryController, :generate
    post "/generate", BirmaWeb.PantryController, :generate_meals
  end

  scope "/api" do
    pipe_through([:api])

    forward "/graphiql", Absinthe.Plug.GraphiQL,
      schema: BirmaWeb.Schema,
      interface: :simple
  end

  # Default Routes for Veil
  scope "/veil", BirmaWeb.Veil do
    pipe_through(:browser)

    get("/users/new", UserController, :new)
    post("/users", UserController, :create)
    get("/sessions/new/:request_unique_id", SessionController, :create)
    get("/sessions/signout/:session_unique_id", SessionController, :delete)
  end
end

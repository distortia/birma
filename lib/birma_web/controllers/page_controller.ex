defmodule BirmaWeb.PageController do
  use BirmaWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def catalog(conn, _params) do
    items = Birma.Pantry.list_items()
    render(conn, "catalog.html", items: items)
  end
end

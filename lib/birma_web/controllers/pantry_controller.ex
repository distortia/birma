defmodule BirmaWeb.PantryController do
  use BirmaWeb, :controller
  alias Birma.Pantry
  plug BirmaWeb.Plugs.Veil.Authenticate when action in [:manage, :generate_meals, :index]
  def index(conn, meals \\ []) do
    # meals = Pantry.generate_meals()
    # meals = Pantry.retrieve_meals(conn.assigns.veil_user_id)
    render(conn, "index.html", meals: meals)
  end

  def manage(conn, _params) do
    render(conn, "manage.html", items: Pantry.list_items())
  end

  # def add(conn, %{"pantry_items" => pantry_items}) do
  #   # add items to my pantry
  #   # Pantry.add_to_user(pantry_items)
  #   redirect(@conn, to: "/index")
  # end

  def generate(conn, _params) do
    render(conn, "generate.html")
  end

  def generate_meals(conn, %{"days" => days}) do
    number_of_days = String.to_integer(days)
    meals = Pantry.generate_meals(number_of_days)
    render(conn, "index.html", meals: meals)
  end
end

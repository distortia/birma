defmodule BirmaWeb.Schema do
  use Absinthe.Schema
  alias BirmaWeb.PantryResolver

  object :item do
    field :id, non_null(:id)
    field :name, non_null(:string)
    field :type, non_null(:string)
  end

  query do
    @desc "Lists all items"
    field :all_items, non_null(list_of(non_null(:item))) do
      resolve &PantryResolver.all_items/3
    end

    @desc "Lits items within a meal"
    field :generate_meal, list_of(non_null(:item)) do
      resolve &PantryResolver.generate_meal/3
    end

    @desc "Returns list of meals"
    field :generate_meals, list_of(list_of(non_null(:item))) do
      resolve &PantryResolver.generate_meals/3
    end
  end

  mutation do
    field :create_item, :item do
      arg :name, non_null(:string)
      arg :type, non_null(:string)

      resolve &PantryResolver.create_item/3
    end
  end
end

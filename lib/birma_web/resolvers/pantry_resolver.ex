defmodule BirmaWeb.PantryResolver do
  alias Birma.Pantry

  def all_items(_root, _args, _info) do
    items = Pantry.list_items()
    {:ok, items}
  end

  def create_item(_root, args, _info) do
    case Pantry.create_item(args) do
      {:ok, item} -> {:ok, item}
      _error -> {:error, "Could not create item"}
    end
  end

  def generate_meal(_root, _args, _info) do
    meal = Pantry.generate_meal()
    {:ok, meal}
  end

  def generate_meals(_root, _args, _info) do
    meals = Pantry.generate_meals()
    {:ok, meals}
  end
end

# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Birma.Repo.insert!(%Birma.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias Birma.Pantry.Item
alias Birma.Repo
alias Birma.Veil.User

user = %User{email: "nickstalter@gmail.com", verified: true} |> Repo.insert!() |> Repo.preload(:items)

[
  %Item{name: "ribeye", type: "meat", amount: 2},
  %Item{name: "brisket", type: "meat", amount: 2},
  %Item{name: "sirloin steak", type: "meat", amount: 2},
  %Item{name: "beef stew", type: "meat", amount: 1},
  %Item{name: "chuck roast", type: "meat", amount: 1},
  %Item{name: "beef stir-fry", type: "meat", amount: 1},
  %Item{name: "ground chuck", type: "meat", amount: 1},
  %Item{name: "pork chops", type: "meat", amount: 2},
  %Item{name: "pork tenderloin", type: "meat", amount: 2},
  %Item{name: "pork steak", type: "meat", amount: 1},
  %Item{name: "chicken thighs", type: "meat", amount: 3},
  %Item{name: "chicken cutlet", type: "meat", amount: 2},
  %Item{name: "chicken stir-fry", type: "meat", amount: 3},
  %Item{name: "chicken wings", type: "meat", amount: 4},
  %Item{name: "spinach", type: "vegetable", amount: 1},
  %Item{name: "potato", type: "vegetable", amount: 2},
  %Item{name: "salad", type: "vegetable", amount: 5},
  %Item{name: "zucchini", type: "vegetable", amount: 2},
  %Item{name: "green beans", type: "vegetable", amount: 1},
  %Item{name: "cauliflower", type: "vegetable", amount: 4},
  %Item{name: "broccoli", type: "vegetable", amount: 2},
  %Item{name: "onion", type: "vegetable", amount: 4},
  %Item{name: "cabbage", type: "vegetable", amount: 5}
] |> Enum.each(fn i ->
  Ecto.build_assoc(user, :items, i)
  |> Repo.insert!()
end)

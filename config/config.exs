# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :birma,
  ecto_repos: [Birma.Repo]

# Configures the endpoint
config :birma, BirmaWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "a5jL2M2wNpJ9Dq2OPLY1Em8B11OjdJeguSZHf1Wm3U48meg6gV2id/fluvbBYrj9",
  render_errors: [view: BirmaWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Birma.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :veil,
  site_name: "Birma",
  email_from_name: "Nick",
  email_from_address: "support@getbirma.com",
  sign_in_link_expiry: 12 * 3_600, # How long should emailed sign-in links be valid for?
  session_expiry: 86_400 * 30, # How long should sessions be valid for?
  refresh_expiry_interval: 86_400,  # How often should existing sessions be extended to session_expiry
  sessions_cache_limit: 250, # How many recent sessions to keep in cache (to reduce database operations)
  users_cache_limit: 100 # How many recent users to keep in cache


config :veil, Brima.Veil.Scheduler,
  jobs: [
    # Runs every midnight to delete all expired requests and sessions
    {"@daily", {Brima.Veil.Clean, :expired, []}}
  ]

  config :veil, BirmaWeb.Veil.Mailer,
  adapter: Swoosh.Adapters.Sendgrid,
  api_key: System.get_env("SENDGRID_KEY") || File.read!("./sendgrid_api_key.txt")

  # -- End Veil Configuration

  # Import environment specific config. This must remain at the bottom
  # of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
